// bài tập 1
//  input: nhập 3 số nguyên
// cách xử lý: tạo từng biến khi người dùng nhập từng số nguyên r so sánh theo thứ tự tăng dần
// output: xuất 3 số theo thứ tự tăng dần

document.getElementById("txt-ket-qua").addEventListener("click", function () {
  var soThu1 = document.getElementById("txt-so1").value * 1;
  var soThu2 = document.getElementById("txt-so2").value * 1;
  var soThu3 = document.getElementById("txt-so3").value * 1;

  if (soThu1 < soThu2 && soThu2 < soThu3) {
    so1 = soThu1;
    so2 = soThu2;
    so3 = soThu3;
    console.log("Kết quả", soThu1, soThu2, soThu3);
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
  } else if (soThu2 < soThu1 && soThu1 < soThu3) {
    so1 = soThu2;
    so2 = soThu1;
    so3 = soThu3;
    console.log("Kết quả", soThu2, soThu1, soThu3);
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
  } else if (soThu2 < soThu3 && soThu3 < soThu1) {
    so1 = soThu2;
    so2 = soThu3;
    so3 = soThu1;
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
    console.log("Kết quả", soThu2, soThu3, soThu1);
  } else if (soThu1 < soThu3 && soThu3 < soThu2) {
    so1 = soThu1;
    so2 = soThu3;
    so3 = soThu2;
    console.log("Kết quả", soThu1, soThu3, soThu2);
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
  } else if (soThu3 < soThu2 && soThu2 < soThu1) {
    so1 = soThu3;
    so2 = soThu2;
    so3 = soThu1;
    console.log("Kết quả", soThu3, soThu2, soThu1);
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
  } else {
    so1 = soThu3;
    so2 = soThu1;
    so3 = soThu2;
    console.log("Kết quả", soThu3, soThu1, soThu2);
    document.getElementById(
      "in-ket-qua"
    ).innerHTML = ` Thứ tự tăng dần : ${so1} ${so2} ${so3}`;
  }
});
// bài tạp 2
// input: người dùng nhập người sử dụng máy tính
//  cách xử lý :lấy dữ liệu khi người dùng nhập người xử dụng máy thì chào người đó
//  output : chào người sử dụng
var bo = "bo";
var me = "me";
var anhTrai = "anhTrai";
var emGai = "emGai";
var cachChao = function (nguoiDung) {
  switch (nguoiDung) {
    case bo:
      return "Bố";
    case me:
      return "Mẹ";
    case anhTrai:
      return "Anh Trai";
    case emGai:
      return "Em Gái";
    default:
      return;
  }
};
document.getElementById("txt-xac-nhan").addEventListener("click", function () {
  var chaoHoi = document.querySelector('input[name="optradio"]:checked').value;
  var xinChao = cachChao(chaoHoi);
  console.log("Xin chào : ", xinChao);
  document.getElementById(
    "txt-xin-chao"
  ).innerHTML = `<div>Xin chào ${xinChao}</div>`;
});
// bài tập 3
// input 3 số nguyên bất kỳ
// cách xử lý: lấy số chẵn bằng cách chia hết cho 2 và dư 0 là số chẵn còn lại là số lẻ
// output : bao nhiêu số chẵn bao nhiêu số lẻ
document.getElementById("ket-qua").addEventListener("click", function () {
  var soNguyen1Value = document.getElementById("txt-soNguyen1").value * 1;
  var soNguyen2Value = document.getElementById("txt-soNguyen2").value * 1;
  var soNguyen3Value = document.getElementById("txt-soNguyen3").value * 1;

  var countSoChan = 0;
  var countSoLe = 0;

  if (soNguyen1Value % 2 == 0) {
    countSoChan++;
  } else {
    countSoLe++;
  }

  if (soNguyen2Value % 2 == 0) {
    countSoChan++;
  } else {
    countSoLe++;
  }

  if (soNguyen3Value % 2 == 0) {
    countSoChan++;
  } else {
    countSoLe++;
  }

  console.log(countSoChan, countSoLe);

  document.getElementById(
    "chan-le"
  ).innerHTML = `<span>Có ${countSoChan} số chẵn, ${countSoLe} số lẻ</span>`;
});
// bài tập 4
// input nhập chiều dài 3 cạnh
// cách xử lý: cho 3 cạnh tương đương a b c
// nếu chiều dài 3 cạnh bằng nhau thì là tam giác đều
// nếu 2 cạnh bằng nhau thì là tam giác cân
// nếu c^2 = a^2+b^2 thì là  tam giác vuông
document.getElementById("txt-tam-giac").addEventListener("click", function () {
  var doDai1value = document.getElementById("txt-canh1").value * 1;
  var doDai2value = document.getElementById("txt-canh2").value * 1;
  var doDai3value = document.getElementById("txt-canh3").value * 1;

  if (
    doDai1value == doDai2value &&
    doDai1value == doDai3value &&
    doDai2value == doDai3value
  ) {
    console.log("tam giác đều");
    document.getElementById(
      "loai-tam-giac"
    ).innerHTML = `<span> Tam Giác Đều </span>`;
  } else if (
    doDai1value == doDai3value ||
    doDai1value == doDai2value ||
    doDai2value == doDai1value ||
    doDai2value == doDai3value ||
    doDai3value == doDai1value ||
    doDai3value == doDai2value
  ) {
    console.log("tam giác cân");
    document.getElementById(
      "loai-tam-giac"
    ).innerHTML = `<span> Tam Giác Cân </span>`;
  } else {
    const dinhLy =
      doDai1value * doDai1value + doDai2value * doDai2value ==
      doDai3value * doDai3value;
    const dinhLy1 =
      doDai3value * doDai3value + doDai2value * doDai2value ==
      doDai3value * doDai3value;

    const dinhLy2 =
      doDai1value * doDai1value + doDai3value * doDai3value ==
      doDai3value * doDai3value;
    if (dinhLy || dinhLy1 || dinhLy2) {
      console.log("tam giác vuông");
      document.getElementById(
        "loai-tam-giac"
      ).innerHTML = `<span> Là Tam Giác Vuông </span>`;
    } else {
      console.log("một tam giác nào đó");
      document.getElementById(
        "loai-tam-giac"
      ).innerHTML = `<span> Một Tam Giác Nào Đó </span>`;
    }
  }
});
